package sbp.duplicate;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collection;

class DuplicateCollectionsTest {

    Integer [] num = {78,2,44,4,5,6,67,98,9,10,51,12,13,13,15,61,5,18,19,67};
    String [] str = {"ABC", "AB", "DCS", "CDW", "ABC", "TRD", "DCS", "OUY"};
    Collection <Integer> collectionNum = Arrays.asList(num);
    Collection <String> collectionStr = Arrays.asList(str);

    /**
     * Передаеь коллекцию в метод findDuplicatesInCollections
     * возвращает коллекцю дубликатов
     */
    @Test
    void findDuplicatesInCol() {

        DuplicateCollections col = new DuplicateCollections();
        System.out.println("Коллекция до поиска дубликатов " + collectionNum);
        Collection value = col.findDuplicatesInCol(collectionNum);
        System.out.println("Дубликаты из коллекции " + value);

    }

    /**
     * Передаеь коллекцию в метод findDuplicatesInColSecond
     * возвращает коллекцю дубликатов
     */
    @Test
    void findDuplicatesInColSecond() {

        DuplicateCollections col = new DuplicateCollections();
        System.out.println("Коллекция до поиска дубликатов " + collectionStr);
        Collection value = col.findDuplicatesInColSecond(collectionStr);
        System.out.println("Дубликаты из коллекции " + value);
    }

    /**
     * Передаеь коллекцию в метод findDuplicatesInColThird
     * возвращает коллекцю дубликатов
     */
    @Test
    void findDuplicatesInColThird() {

        DuplicateCollections col = new DuplicateCollections();
        System.out.println("Коллекция до поиска дубликатов " + collectionStr);
        Collection value = col.findDuplicatesInColThird(collectionStr);
        System.out.println("Дубликаты из коллекции " + value);
    }
}