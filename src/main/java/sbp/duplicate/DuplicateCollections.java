package sbp.duplicate;

import java.util.*;
import java.util.stream.Collectors;

public class DuplicateCollections {
    /**
     * Принамае Collection<T> и возвращант коллекцию дубликатов
     * @param collection
     * @param <T>
     * @return
     */
    public <T> Collection <T> findDuplicatesInCol (Collection<T> collection) {
        final Collection<T> duplicatedObjects = new ArrayList<T>();
        Set<T> set = new HashSet<T>() {
            @Override
            public boolean add(T e) {
                if (contains(e)) {
                    duplicatedObjects.add(e);
                }
                return super.add(e);
            }
        };
        for (T t : collection) {
            set.add(t);
        }
        return duplicatedObjects;
    }

    /**
     * Принамае Collection<T> и возвращант коллекцию дубликатов
     * @param collection
     * @param <T>
     * @return
     */
    public <T> Set<T> findDuplicatesInColSecond(Collection<T> collection) {
        Set<T> elements = new HashSet<>();
        return collection.stream()
                .filter(e -> !elements.add(e))
                .collect(Collectors.toSet());
    }

    /**
     * Принамае Collection<T> и возвращант коллекцию дубликатов
     * @param collection
     * @param <T>
     * @return
     */
	public static <T> Set <T> findDuplicatesInColThird (Collection<T> collection) {
        List <T> a = new ArrayList<>(collection);
	    Set<T> duplicates = new HashSet<>();
	           for (int i = 0; i < collection.size(); i++) {
	              T e1 = a.get(i);
	                 if (e1 == null) continue;
	                      for (int j = 0; j < a.size(); j++) {
	                          if (i == j) continue;
	                               T e2 = a.get(j);
	                                  if (e1.equals(e2)) {
	                                      duplicates.add(e2);
	                                  }
	                      }
	           }
	        return duplicates;
	}

}
