package sbp.workDB;

import sbp.CRUD.CRUD;
import sbp.comparator.Person;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionDB {

    public static String Url = "jdbc:sqlite:C:\\Users\\Владислав\\Desktop\\обучение\\example\\lecture4\\src\\main\\java\\sbp\\workDB\\Persons.db";

    public static void main(String[] args) throws SQLException {


        Connection connection = DriverManager.getConnection(Url);
        System.out.println(connection);

        CRUD action = new CRUD(connection);
        Person person1 = new Person("Anton", "Kaliningtad", 37);

        //action.deletePerson("Vladislav");
        //action.findAll();
        //action.creatTable();
        action.addPerson(person1);
        //action.changePersonName("Vladislav","Anton");
        //action.dropTable();
    }

}
