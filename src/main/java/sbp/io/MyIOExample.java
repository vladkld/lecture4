package sbp.io;


import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.*;
import java.nio.file.attribute.FileTime;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.Date;

import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;

public class MyIOExample
{

    /**
     * Создать объект класса {@link }, проверить существование.
     * Если сущность существует, то вывести в консоль информацию:
     *      - абсолютный путь
     *      - родительский путь
     * Если сущность является файлом, то вывести в консоль:
     *      - размер
     *      - время последнего изменения
     * Необходимо использовать класс {@link }
     * @return - true, если файл успешно создан
     */
    public boolean workWithFile(String  filename) throws IOException{

        Path file = Paths.get(filename);
        System.out.println("Это директория " + Files.isDirectory(file));
        System.out.println("Это файл " + Files.isRegularFile(file));
        System.out.println("Абсолютный путь " + file.toAbsolutePath());
        System.out.println("Родительский путь " + file.getParent());

            if (Files.exists(Paths.get(String.valueOf(file)))) {
                         System.out.println("Размер файла " + Files.size(file) + " символов");
                         FileTime lastModifiedTime = Files.getLastModifiedTime(file);
                         String input = String.valueOf(lastModifiedTime);
                         Date date = Date.from(Instant.parse(input));
                         SimpleDateFormat outFormatter = new SimpleDateFormat("dd-MM-yyyy");
                         String lastData = outFormatter.format(date);
                         System.out.println("Прследняя модификация " + lastData);
                             return true;
            } else {
                    System.out.println("Файла не существует");
            }
            Files.createFile(file);
        return false;
    }

    /**
     * Метод должен создавать копию файла
     * @param sourceFileName - имя исходного файла
     * @param destinationFileName - имя копии файла
     * @return - true, если файл успешно скопирован
     */
    public boolean copyFile(String sourceFileName, String destinationFileName)  throws IOException{
        /**
        * Создаётся копия ранее созданного фаёла с помощью классов java.io.FileInputStream и java.io.FileOutputStream
         */
            Path fileIs = Paths.get(sourceFileName);
            Path fileOs = Paths.get(destinationFileName);
               if (Files.exists(fileIs)) {
                   InputStream reader = Files.newInputStream(fileIs);
                   try {
                       OutputStream writer = Files.newOutputStream(fileOs, StandardOpenOption.WRITE); {
                           while (reader.read() > 0) {
                               writer.write(reader.read());
                           }
                       }
                   } catch(IOException e) {
                       throw new RuntimeException(e);
                   }
               }
               return true;
    }


    /**
     * Метод создает копию файла
     * @param sourceFileName - имя исходного файла
     * @param destinationFileName - имя копии файла
     * @return - true, если файл успешно скопирован
     */
     public boolean copyBufferedFile(String sourceFileName, String destinationFileName) throws IOException {

           Path fileIs = Paths.get(sourceFileName);
           Path fileOs = Paths.get(destinationFileName);

               if (Files.exists(fileIs)) {
                  BufferedReader reader = Files.newBufferedReader(fileIs, StandardCharsets.UTF_8);

                  try (BufferedWriter writer = Files.newBufferedWriter(fileOs, StandardCharsets.UTF_8,
                         StandardOpenOption.WRITE)){

                         while (reader.readLine() != null) {
                             writer.write(reader.readLine());
                             writer.newLine();
                         }

                 } catch(IOException e) {
                    throw new RuntimeException(e);
                 }
             }
               return true;
   }

    /**
     * Метод создаёт копию файла
     * Использован nio2
     * @param sourceFileName - имя исходного файла
     * @param destinationFileName - имя копии файла
     * @return - true
     */
    public boolean copyFileWithReaderAndWriter(String sourceFileName, String destinationFileName) {
        Path source = Paths.get(sourceFileName);
        Path dest = Paths.get(destinationFileName);
               try {
                  Files.copy(source, dest, REPLACE_EXISTING);
               } catch (IOException e) {
                          e.printStackTrace();
               }
             return true;
    }
}
