package sbp.io;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

import java.io.IOException;

class MyIOExampleTest {

    @Test
    void workWithFileTest() throws IOException {
        /**
         * workWithFileTest вызывает workWithFile из класса MyIOExample
         * сравнивает возвращаемое значение от workWithFile
         */
        MyIOExample test = new MyIOExample();
        Assert.assertTrue(test.workWithFile("C:/Users/Владислав/Desktop/обучение/example/lecture4/MyFile.txt"));
    }

    @Test
    void copyFileTest() throws IOException {
        /**
         * copyFileTest вызывает copyFile из класса MyIOExample
         * сравнивает возвращаемое значение от copyFile
         */
        MyIOExample test = new MyIOExample();
        Assert.assertTrue(test.copyFile("C:/Users/Владислав/Desktop/обучение/newTest.txt", "C:/Users/Владислав/Desktop/обучение/example/lecture4/MyFile.txt"));
    }

      @Test
       void copyBufferedFileTest() throws IOException {
        /**
         * copyBufferedFileTest вызывает copyBufferedFile из класса MyIOExample
         * сравнивает возвращаемое значение от copyBufferedFile
         */
          MyIOExample test = new MyIOExample();
          Assert.assertTrue(test.copyBufferedFile("C:/Users/Владислав/Desktop/обучение/newTest.txt", "C:/Users/Владислав/Desktop/обучение/example/lecture4/MyFile.txt"));

       }
        @Test
        void copyFileWithReaderAndWriterTest() {
        /**
         * copyFileWithReaderAndWriterTest вызывает copyFileWithReaderAndWriter из класса MyIOExample
         * сравнивает возвращаемое значение от copyFileWithReaderAndWriter
         */
        MyIOExample test = new MyIOExample();
        Assert.assertTrue(test.copyFileWithReaderAndWriter("C:/Users/Владислав/Desktop/обучение/newTest.txt", "C:/Users/Владислав/Desktop/обучение/example/lecture4/MyFile2.txt"));
   }
}