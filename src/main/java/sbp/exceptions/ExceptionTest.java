package sbp.exceptions;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class ExceptionTest extends WorkWithExceptions{

    @Test
    void  exceptionProcessingTest () {

        /**
          В тесте проверяется факт подброски исключения в exceptionProcessing()
         и отработка exceptionProcessing()
         */
        WorkWithExceptions test = new WorkWithExceptions();
        Assertions.assertThrows(CustomException.class, () -> exceptionProcessing());
    }
}
