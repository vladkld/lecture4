package sbp.CRUD;

import sbp.comparator.Person;
import sbp.workDB.ConnectionDB;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;


public class CRUD {

    private final Connection connection;

    public CRUD(Connection connection)  {
            this.connection = connection;
    }
    /**
     * Метод creatTable() создает таблицу Persons в базе данных Persons.db
     */
    public void creatTable () {
        String sql = "create table Persons (name VARCHAR(20), city VARCHAR (20), age INTEGER);";
        try (Statement statement = connection.createStatement();)
        { int affectedRows = statement.executeUpdate(sql);
            System.out.println("Добавлено записей " + affectedRows);
       } catch (SQLException ex){
            ex.printStackTrace();
        }
    }

    /**
     * Метод dropTable() удаляет таблицу Persons в базе данных Persons.db
     */
    public void dropTable (){
        try (Statement statement = connection.createStatement();)
        { int affectedRows = statement.executeUpdate("drop table Persons");
            System.out.println("Таблица удалена " + affectedRows);
        }catch (SQLException ex){
            ex.printStackTrace();
        }
    }

    /**
     * Метод addPerson(Person person) добаляет запись в таблицу Persons
     * @param person
     */
    public void addPerson(Person person) {
        String sql = String.format("insert into Persons ('name', 'city', 'age') VALUES ('%s', '%s', '%s')",
                person.getName(), person.getCity(), person.getAge());
        try (Statement statement = connection.createStatement();)
        { int affectRows = statement.executeUpdate(sql);
            System.out.println("affectRows = " + affectRows);
        } catch (SQLException ex){
            ex.printStackTrace();
        }
    }

    /**
     * Метод changePersonName(String name, String newName) меняет значение в поле name таблицы Persons
     * @param name
     * @param newName
     */
    public void changePersonName(String name, String newName){
        try (Statement statement = connection.createStatement();)
        {int affectedRows = statement.executeUpdate("update Persons set name = '" + newName + "'where name ='" + name + "';");
            System.out.println("changePersonName " + affectedRows);
        }catch (SQLException ex){
            ex.printStackTrace();
        }
    }

    /**
     * Метод findAll() выводит все значения из таблицы Persons
     */
    public void findAll(){
        try (Statement statement = connection.createStatement();)
        { String sql = "select name, city, age from Persons";
            ResultSet rs = statement.executeQuery(sql);
            while (rs.next()) {
                System.out.println(rs.getObject("name") + "\t"
                        + rs.getObject("city") + "\t"
                        + rs.getObject("age"));
            }
        } catch (SQLException ex){
            ex.printStackTrace();
        }
    }
    /**
     * Метод deletePerson(String name) удаляет строку из таблицы Persons
     * @param name
     */
    public void deletePerson(String name){
        try (Statement statement = connection.createStatement();)
        {int affectedRows = statement.executeUpdate("delete from Persons where name = '" + name + "';");
            System.out.println("affectedRows= " + affectedRows);
        } catch (SQLException ex){
            ex.printStackTrace();
        }
    }

}
