package sbp.CRUD;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import sbp.comparator.Person;

import java.sql.*;

class CRUDTest {

    public static String Url = "jdbc:sqlite:C:\\Users\\Владислав\\Desktop\\обучение\\example\\lecture4\\src\\main\\java\\sbp\\workDB\\Persons.db";

    /**
     * Вызывает метод creatTable() для создания таблицы Persons в базе данных Persons.db
     */
    @Test
    void creatTable() {
        try (Connection connection = DriverManager.getConnection(Url);)
        { CRUD action = new CRUD(connection);
            action.creatTable();
        } catch (SQLException ex){
            ex.printStackTrace();
        }
    }

    /**
     * Вызывает метод dropTable() для удаления таблицы Persons в базе данных Persons.db
     */
    @Test
    void dropTable(){
       try (Connection connection = DriverManager.getConnection(Url);)
       { CRUD action = new CRUD(connection);
           action.dropTable();
       } catch (SQLException ex){
           ex.printStackTrace();
       }
    }

    /**
     * вызывает метод addPerson()
     * сравнивает количество записей до выполнения метода и после
     */
    @Test
    void addPerson() {
       try (Connection connection = DriverManager.getConnection(Url);)
       { CRUD action = new CRUD(connection);
           Person person1 = new Person("Anton", "Kaliningtad", 37);
           Statement stmt3 = connection.createStatement();
           ResultSet rs3 = stmt3.executeQuery("SELECT COUNT(*) AS count FROM Persons ;");
           int count = rs3.getInt("count");
           action.addPerson(person1);
           ResultSet rs2 = stmt3.executeQuery("SELECT COUNT(*) AS count FROM Persons ;");
           int countNew = rs2.getInt("count");
           int temp = countNew - count;
           Assertions.assertEquals(count + temp, countNew);
       } catch (SQLException ex){
           ex.printStackTrace();
       }
    }

    /**
     * вызывает changePersonName()
     */
    @Test
    void changePersonName() {
       try (Connection connection = DriverManager.getConnection(Url);)
       { CRUD action = new CRUD(connection);
           System.out.println("Таблица до выполнения метода changePersonName()");
           action.findAll();
           action.changePersonName("Anton", "Vladisla");
           System.out.println("Таблица после выполнения метода changePersonName()");
           action.findAll();
       }catch (SQLException ex){
           ex.printStackTrace();
       }
    }

    /**
     * вызывает метод findAll()
     */
    @Test
    void findAll() {
       try (Connection connection = DriverManager.getConnection(Url);)
       { CRUD action = new CRUD(connection);
           action.findAll();
       } catch (SQLException ex) {
           ex.printStackTrace();
       }
    }

    /**
     * вызывает метод deletePerson()
     * сравнивает количество записей до выполнения метода и после
     */
    @Test
    void deletePerson(){
       try (Connection connection = DriverManager.getConnection(Url);)
       { CRUD action = new CRUD(connection);
           Statement stmt3 = connection.createStatement();
           ResultSet rs3 = stmt3.executeQuery("SELECT COUNT(*) AS count FROM Persons ;");
           int count = rs3.getInt("count");
           action.deletePerson("Anton");
           ResultSet rs2 = stmt3.executeQuery("SELECT COUNT(*) AS count FROM Persons ;");
           int countNew = rs2.getInt("count");
           int temp = count - countNew ;
           Assertions.assertEquals(count - temp, countNew);
       } catch (SQLException ex){
           ex.printStackTrace();
       }
    }
}