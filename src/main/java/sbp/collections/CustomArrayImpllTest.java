package sbp.collections;

import org.junit.Assert;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collection;

class CustomArrayImpllTest {

    Integer[] intList = {1, 2, 7, 11, 23, 23, 12, 23,3,7,9};
    Collection collection = Arrays.asList(intList);
    Integer integer = 2;

    /**
     * Тест сравнение размкра массива
     * метод size
     */
    @Test
    void size() {

        CustomArrayImpll customArray = new CustomArrayImpll();
        Assert.assertEquals (0, (customArray.size()));
    }

    /**
     * Тест проверяет наличие елементов в массиве
     * метод isEmpty
     */
    @Test
    void isEmpty() {

       CustomArrayImpll  emptyArray = new CustomArrayImpll();
       System.out.print("Массив пуст " + emptyArray.isEmpty());
       Assert.assertFalse(emptyArray.isEmpty());
    }
    /**
     * Тест добавляет элемент в массив
     * метод add
     */
    @Test
    void add() {
     CustomArrayImpll <Integer> addArray = new CustomArrayImpll();

     Assert.assertTrue(addArray.add(3));
     System.out.println(addArray.size());
    }
    /**
     * Тест добавляет элементов в массив
     * метод add
     */
    @Test
    void addAll() {
        CustomArrayImpll  addArray = new CustomArrayImpll();
        Assert.assertTrue(addArray.addAll(intList));
        System.out.println(addArray.size());
    }

    /**
     * Тест добавляет элементов в массив
     * метод add
     */
    @Test
    void testAddAll() {
        CustomArrayImpll  addArray = new CustomArrayImpll();
        Assert.assertTrue(addArray.addAll(collection));
        System.out.println(addArray.size());
    }

    /**
     * Тест добавляет элементов в массив
     * метод add
     */
    @Test
    void testAddAll1() {
        CustomArrayImpll  addArray = new CustomArrayImpll();
        Assert.assertTrue(addArray.addAll(0, intList));
    }
    /**
     * Тест выхова элемента массива
     * метод get
     */
    @Test
    void get() {
        CustomArrayImpll  getArray = new CustomArrayImpll();
        Assertions.assertEquals(null,getArray.get(8));
    }

    /**
     * Проверка результата кдаления элемента из массива ро индексу
     */
    @Test
    void remove() {
        CustomArrayImpll  removeArray = new CustomArrayImpll();
        Assert.assertEquals(0, (removeArray.size() ));
    }
    /**
     * Проверка результата кдаления элемента из массива по элементк
     */
    @Test
    void testRemove() {
        CustomArrayImpll  removeArray = new CustomArrayImpll();
        Assertions.assertTrue(removeArray.remove(integer));
    }
    /**
     * Тест сравнение
     */
    @Test
    void contains() {
        CustomArrayImpll  comtArray = new CustomArrayImpll();
        Assertions.assertFalse(comtArray.contains(integer));
    }

    /**
     * Возвращает первый совпадающий индекс
     */
    @Test
    void indexOf() {
        CustomArrayImpll  indexOfArray = new CustomArrayImpll();
        Assertions.assertEquals(-1,indexOfArray.indexOf(integer));
    }
    /**
     * Тест увеличения массива при необходимости
     */
    @Test
    void ensureCapacity() {
        CustomArrayImpll customArray = new CustomArrayImpll();
        Assertions.assertEquals(11,customArray.size() + intList.length);
    }
    /**
     * Тест проверки емкости массива
     */
    @Test
    void getCapacity() {
        CustomArrayImpll capacity = new CustomArrayImpll();
        Assert.assertEquals(10,capacity.getCapacity());
    }

}