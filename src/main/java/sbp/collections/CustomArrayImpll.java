package sbp.collections;


import java.util.Collection;
import java.util.Objects;

public class CustomArrayImpll<T> implements CustomArray<T> {


    private T[] customArray;
    private int size;

    /**
     * Инициализация customArray и size
     */

    public CustomArrayImpll() {
        this.customArray = (T[]) new Object[10];
        this.size = 0;
    }
    /**
     * принимаем значение  и подбрасывает его
     * @param capacity
     * @throws IllegalArgumentException
     */
    public CustomArrayImpll(int capacity) {

        try {
            this.customArray = (T[]) new Object[capacity];
            this.size = 0;

        } catch (RuntimeException e) {
            System.out.println("Unchecked exception" + e.getMessage());
            e.getStackTrace();
            throw new IllegalArgumentException("Ошибка!", e);
        } catch (Exception e) {
            System.out.println("Checked exception." + e.getMessage());
            e.getStackTrace();
        }
    }
    /**
     * Принимаем значение
     * @param c
     */

    public CustomArrayImpll(Collection<T> c)
    {
        this(c.size());
        c.toArray(customArray);
        this.size = c.size();
    }

    /**
     * возвращает размер array
     *
     * @return
     */
    @Override
    public int size() {
        return this.size;
    }

    /**
     * возвращает true при
     *
     * @return
     */
    @Override
    public boolean isEmpty() {
        if (this.customArray== null || this.customArray.length == 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Добавляет новый элемент в масссив При достижении размера внутреннего
     * массива происходит его увеличение на 5.
     */

    @Override
    public boolean add(T item) {

        if (Objects.nonNull(item)) {
            ensureCapacity(this.size + 1);
            this.customArray[this.size++ ] = item;
        return true;
        }
        return false;
    }
    /**
     * Добавляет элементы
     *
     * @throws IllegalArgumentException if parameter items is null
     */

    @Override
    public boolean addAll(T[] items) {

        T[] temp = (T[]) new Object[items.length];
        if (customArray.length == 0) {
            throw new IllegalArgumentException();
        }
           if ( customArray.length < size() + items.length) {
              ensureCapacity(size() + temp.length);

           }
                for (T o : temp) {
                 customArray[size++] = o;
                }
        return true;
    }
    /**
     * Добавляет элементы коллекции
     *
     * @throws IllegalArgumentException if parameter items is null
     */
    @SuppressWarnings("unchecked")
    @Override
    public boolean addAll(Collection<T> items) {

        T[] temp = items.toArray((T[])new Object[items.size()]);
        this.size = 0;
        if (customArray.length == 0) {
                throw new IllegalArgumentException();
        }
        return addAll(temp);
    }

    /**
     * Добавляет элемент на указанное место в массиве
     *
     * @param index - index
     * @param items - items for insert
     * @throws ArrayIndexOutOfBoundsException if index is out of bounds
     * @throws IllegalArgumentException       if parameter items is null
     */
    @SuppressWarnings("unchecked")
    @Override
    public boolean addAll(int index, T[] items) {
        customArray = items;
        int indexNum = customArray.length;
        T[] newArray = (T[]) new Object[items.length];
        if (indexNum == 0) {
            throw new IllegalArgumentException();
        }
        if (customArray.length - 1 < index) {
            throw new IllegalArgumentException();
        }
            if (customArray.length <= size) {
                    newArray = (T[]) new Object[customArray.length + 5];
                    System.arraycopy(customArray, 0, newArray, 0, customArray.length);
                    customArray = newArray;
            }
        System.arraycopy(customArray, 0, newArray, index, indexNum);
        size += indexNum;
        return true;
    }

    /**
     * Подучает значение по index
     *
     * @param index - index
     * @throws ArrayIndexOutOfBoundsException if index is out of bounds
     */
    @Override
    public T get(int index) {

        if (customArray.length - 1 < index) {
            throw new ArrayIndexOutOfBoundsException();
        }
        return (T)customArray[index];
    }

    /**
     * Устанавливаеи значение по index.
     *
     * @param index - index
     * @return old value
     * @throws ArrayIndexOutOfBoundsException if index is out of bounds
     */
    @Override
    public T set(int index, T item) {
        if (customArray.length - 1 < index) {
            throw new ArrayIndexOutOfBoundsException();
        }
        customArray[index] = item;
        return customArray[index];
    }

    /**
     * Удаляет элемент списка по индексу. Все элементы справа от удаляемого
     * перемещаются на шаг влево.
     */
    @SuppressWarnings("unchecked")
    @Override
    public void remove(int index) {
        T[] newArray = (T[]) new Object[customArray.length];
        if (customArray.length - 1 < index) {
            throw new ArrayIndexOutOfBoundsException();
        }
        if (size - index > 0) {
            System.arraycopy(customArray, index + 1, newArray, index, size );
            customArray = newArray;
            size--;
        } else {
            customArray[size] = null;
        }
    }

    /**
     * удаляет элемент по указанному индексу
     *
     * @param item - item
     * @return
     */
    @Override
    public boolean remove(T item) {
        int indexNum = 0;
        for (int i = 0; i < customArray.length; i++) {
            if (customArray[i] == item) {
                indexNum = i;
                break;
            }
        }
        customArray[indexNum] = null;
        return customArray[indexNum] == null;
    }

    /**
     * Проверяет наличие присваемого значения присылаемое значение
     *
     * @param item - item
     * @return
     */
    @Override
    public boolean contains(T item) {

        for (int i = 0; i < customArray.length; i++) {
            if (customArray[i] == item) {
                break;
            } else {
                return false;
            }
        }
        return true;
    }

    /**
     * Индекс item.
     *
     * @param item - item
     * @return Индекс элемента или -1 если элнмент отсутствунт
     */
    @Override
    public int indexOf(T item) {

        int indexNum = 0;

        for (int i = 0; i < customArray.length; i++) {
            if (customArray[i] == item) {
                indexNum = i;
                break;
            } else {
                return -1;
            }
        }
        return indexNum;
    }

    /**
     * Увеличивает размер массива при необхожимости
     *
     * @param newElementsCount - счётчик элементов
     */
    @SuppressWarnings("unchecked")
    @Override
    public void ensureCapacity(int newElementsCount) {

        if (customArray.length <  newElementsCount + this.size) {
            T[] newArray = (T[]) new Object[newElementsCount + this.size];
            customArray = newArray;
        }
    }

    /**
     * Показывает ёмкость массива
     */
    @Override
    public int getCapacity() {
        return this.customArray.length;
    }

    /**
     * Возвращает массив с расположениям элементов наоборот
     */
    @SuppressWarnings("unchecked")
    @Override
    public void reverse() {
        T[] newArray = (T[]) new Object[customArray.length];
        size = customArray.length - 1;
        for (int i = 0; i < customArray.length - 1; i++) {
            newArray[size] = customArray[i];
            size--;
        }
        customArray = newArray;
    }

    /**
     * озвращает строку
     */
    public String toString() {

        return "data = " + customArray.length + " size = " + size;
    }

    /**
     * создает копию массива
     *
     * @return
     */
    @SuppressWarnings("unchecked")
    @Override
    public T[] toArray() {
        T[] newArray = (T[]) new Object[customArray.length];
        System.arraycopy(customArray, 0, newArray, 0, customArray.length);
        return customArray;
    }

}
