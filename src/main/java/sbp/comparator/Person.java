package sbp.comparator;


public class Person implements Comparable <Person> {


    private String name;
    private String city;
    private int age;

    public Person (String name, String city, int age) {
        this.name = name;
        this.city = city;
        this.age = age;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public String getCity() {
        return city;
    }

    public int getAge() {
        return age;
    }


    /**
     * Сортировка по полю City, затем сортировка по полю name
     * @param other
     * @return
     */
    @Override
    public int compareTo(Person other){
        if (this.city == null) {
            city = "City не указан";
        }
        return this.city.compareToIgnoreCase(other.city) + this.name.compareToIgnoreCase(other.name);
    }


    /**
     * Определение метода toString
     * @return
     */
    @Override
    public String toString() {
        return "Person{" +
                "name = " + name + '\'' +
                ", city = " + city + '\'' +
                ", age = " + age + '\'' +
        '}';
    }


}
