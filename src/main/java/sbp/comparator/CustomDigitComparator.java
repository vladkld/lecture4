package sbp.comparator;

import java.util.Comparator;

public class CustomDigitComparator implements Comparator <Integer> {
    /**
     * Сравнение чётных и нечётных чисел, сортирует по возрастанию
     *
     * @param o1
     * @param o2
     * @return
     * @throws NullPointerException
     */
    @Override
    public int compare(Integer o1, Integer o2) {

        if (o1 % 2 == o2 % 2) {
                return o1.compareTo(o2);
        }
        if (o1 % 2 == 0) {
            return -1;
        }
        return 1;
    }

}