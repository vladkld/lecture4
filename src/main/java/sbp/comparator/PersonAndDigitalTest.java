package sbp.comparator;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;


public class PersonAndDigitalTest {
    Integer [] testIteger = {78,2,44,4,5,6,67,98,9,10,51,12,13,34,15,61,52,18,19,20};
    List <Integer> arrayList = Arrays.asList(testIteger);
    /**
     * Добавляет 3 Person и вызывает метод compareTo()
     */
    @Test
    void compareTo() {
        List<Person> personList = new ArrayList<>();
        personList.add(new Person("Vladislav", "Kaliningrad", 25));
        personList.add(new Person("Aleksandr", "Abakan", 30));
        personList.add(new Person("Anton", "Kaliningrad", 21));
        System.out.println("Befor " + personList);
        personList.sort(Person::compareTo);
        System.out.println("After " + personList);
    }

    /**
     * Передает список в метод compare()
     */
    @Test
    void compare() {
        System.out.println("Befor " + arrayList);
        Collections.sort(arrayList, new CustomDigitComparator());
        System.out.println("After " + arrayList);
    }
}
