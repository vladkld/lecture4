package sbp;

/**
 Пользователь услуг автомойки (Класс в разработке)
 */

public class User {
    private int id;
    private String  name;
    private String surname;
    private String  gender;
    private String car;
    private String  phoneNumber;
    private String  numberCar;


    public static void main(String[] args) {


    }

    public int getId () {return id;}
    public  void setId(int id) {this.id = id;}

    public String getName() {
        return name;
    }
    public void setName(String name) {this.name = name;}

    public String getSurname() {
        return surname;
    }
    public void setSurname(String surname) {this.surname = surname;}

    public String getGender() {
        return gender;
    }
    public void setGender(String gender) {this.gender = gender;}

    public String getCar() {
        return car;
    }
    public void setCar(String car) {this.car = car;}

    public String getPhoneNumber() {
        return phoneNumber;
    }
    public void setPhoneNumber(String phoneNumber) {this.phoneNumber = phoneNumber;}

    public String  getNumberCar() {
        return numberCar;
    }
    public  void setNumberCar(String numberCar) {this.numberCar = numberCar;}

    @Override
    public boolean equals (Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;

        User person = (User) obj;
        return  id == person.id;
    }

    @Override
    public int hashCode () {
        return  id;
    }

    @Override
    public String toString() {
        return String.valueOf(id);
    }
}
