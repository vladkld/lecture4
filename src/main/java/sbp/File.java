package sbp;

import sbp.io.MyIOExample;

import java.util.Scanner;


public class File {



    public static void main(String[] args) throws Exception {

        /**
          С помощью метода main вызываются методы из класса MyIOExample
         */

        System.out.println("Работа с файлом введите 1");
        System.out.println("Копирование файла с помощью Stream 2");
        System.out.println("Копирование файла с помощью BufferedStream 3");
        System.out.println("Копирование файла с помощью File 4");
        Scanner in = new Scanner(System.in);
        System.out.print("Введите чило ");
        int num = in.nextInt();

        switch (num) {

            case 1:
                MyIOExample file = new MyIOExample();
                file.workWithFile("MyFile");
                break;
            case 2:
                MyIOExample file1 = new MyIOExample();
                file1.copyFile("sourceFileName", "destinationFileName");
                System.out.println("Файл скопирован ");
                break;
            case 3:
               MyIOExample file2 = new MyIOExample();
                file2.copyBufferedFile("sourceFileName", "destinationFileName");
               System.out.println("Файл скопирован ");
               break;
           case 4:
               MyIOExample file3 = new MyIOExample();
               file3.copyFileWithReaderAndWriter("sourceFileName", "destinationFileName");
                System.out.println("Фаёл скопирован");
             break;
             default:
              System.out.println("Введено некорректное значение");
               break;
        }
    }
}
